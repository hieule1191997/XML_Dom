﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using DTO;
using DAL;
using BUS;
namespace XMLDOm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        xml_BUS bus = new xml_BUS();
        xml_DAL dal = new xml_DAL();
        XML_DTO dto = new XML_DTO();
        // đưa dữ liệu len combobox
        private void load_combobox()
        {
            DataSet dts = new DataSet();
            dts.ReadXml(dal.path);
            chinhanh_cbb.DataSource = dts.Tables["cuocgoi"];
            chinhanh_cbb.DisplayMember = "chinhanh";
            sogoidi_cbb.DataSource = dts.Tables["cuocgoi"];
            sogoidi_cbb.DisplayMember = "sodien";
        }
        // hiển thị dữ liệu lên listview
        private void hienthi()
        {
            lstview.Items.Clear();
            DataSet dts = new DataSet();
            DataTable dtb = new DataTable();
            dts.ReadXml(dal.path);
            dtb = dts.Tables["cuocgoi"];
            if (dtb.Rows.Count > 0)
            {
                int i = 0;
                foreach (DataRow dr in dtb.Rows)
                {
                    ListViewItem liv = new ListViewItem(dtb.Rows[i]["chinhanh"].ToString());
                    liv.SubItems.Add(dtb.Rows[i]["sodien"].ToString());
                    liv.SubItems.Add(dtb.Rows[i]["sogoiden"].ToString());
                    liv.SubItems.Add(dtb.Rows[i]["ngaygoi"].ToString());
                    liv.SubItems.Add(dtb.Rows[i]["sophut"].ToString());
                    i++;
                    lstview.Items.Add(liv);
                }
            }
            else
            {
                MessageBox.Show("không có dữ liệu để hiển thị", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        //load dư liệu cho form 
        private void Form1_Load(object sender, EventArgs e)
        {
            load_combobox();
            hienthi();

        }

        //đưa dữ liệu từ listview lên các ô text

        private void lstview_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem dr in lstview.SelectedItems)
            {
                chinhanh_cbb.Text = dr.SubItems[0].Text;
                sogoidi_cbb.Text = dr.SubItems[1].Text;
                txbsogoiden.Text = dr.SubItems[2].Text;
                txbngaygoi.Text = dr.SubItems[3].Text;
                txbsophut.Text = dr.SubItems[4].Text;
            }
        }
        //them node cho cuoc goi moi
       
        // bnt them
        private void btnthem_Click(object sender, EventArgs e)
        {
            try
            {
                if (chinhanh_cbb.Text == "" || sogoidi_cbb.Text == "" || txbsogoiden.Text == "" || txbngaygoi.Text == "" || txbsophut.Text == "")
                {
                    MessageBox.Show("vui lòng điền đầy đu thông tin muốn thêm", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    bus.them(chinhanh_cbb.Text, sogoidi_cbb.Text, txbsogoiden.Text, txbngaygoi.Text, txbsophut.Text);
                    hienthi();
                }
            }
            catch(Exception)
            {
                MessageBox.Show("có lỗi xảy ra không thể thêm", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //bnt sua
        private void btnsua_Click(object sender, EventArgs e)
        {
            try
            {
                if ( txbsogoiden.Text == "")
                {
                    MessageBox.Show("Vui lòng nhập số điện thoại muốn sửa", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    bus.sua(chinhanh_cbb.Text, sogoidi_cbb.Text, txbsogoiden.Text, txbngaygoi.Text, txbsophut.Text);
                    hienthi();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("có lỗi xảy ra không thể sửa", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        // bnt xóa

        private void btnxoa_Click(object sender, EventArgs e)
        {
                try
                {
                    if (txbsogoiden.Text == "")
                    {
                        MessageBox.Show("Vui lòng nhập số điện thoại muốn xoá", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        bus.xoa(txbsogoiden.Text);
                        hienthi();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("có lỗi xảy ra không thể xóa", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
        }

        // btn thoat
        private void btnthoat_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Bạn có thự sự muốn thoát chương trình ?", "thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dr == DialogResult.OK)
            {
                Application.Exit();

            }
        }
    }
}
