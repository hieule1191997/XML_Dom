﻿namespace XMLDOm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chinhanh_cbb = new System.Windows.Forms.ComboBox();
            this.sogoidi_cbb = new System.Windows.Forms.ComboBox();
            this.txbsogoiden = new System.Windows.Forms.TextBox();
            this.txbngaygoi = new System.Windows.Forms.TextBox();
            this.txbsophut = new System.Windows.Forms.TextBox();
            this.btnthem = new System.Windows.Forms.Button();
            this.btnsua = new System.Windows.Forms.Button();
            this.btnxoa = new System.Windows.Forms.Button();
            this.btnthoat = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.lstview = new System.Windows.Forms.ListView();
            this.cl1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cl2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cl3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cl4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cl5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(71, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(483, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "CẬP NHẬT THÔNG TIN CUỘC GỌI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(115, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Chi nhánh";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(115, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Số gọi đi ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(115, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Số gọi đến";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(115, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ngày gọi";
            // 
            // chinhanh_cbb
            // 
            this.chinhanh_cbb.FormattingEnabled = true;
            this.chinhanh_cbb.Location = new System.Drawing.Point(220, 93);
            this.chinhanh_cbb.Name = "chinhanh_cbb";
            this.chinhanh_cbb.Size = new System.Drawing.Size(189, 21);
            this.chinhanh_cbb.TabIndex = 5;
            // 
            // sogoidi_cbb
            // 
            this.sogoidi_cbb.FormattingEnabled = true;
            this.sogoidi_cbb.Location = new System.Drawing.Point(220, 120);
            this.sogoidi_cbb.Name = "sogoidi_cbb";
            this.sogoidi_cbb.Size = new System.Drawing.Size(189, 21);
            this.sogoidi_cbb.TabIndex = 6;
            // 
            // txbsogoiden
            // 
            this.txbsogoiden.Location = new System.Drawing.Point(220, 147);
            this.txbsogoiden.Name = "txbsogoiden";
            this.txbsogoiden.Size = new System.Drawing.Size(189, 20);
            this.txbsogoiden.TabIndex = 7;
            // 
            // txbngaygoi
            // 
            this.txbngaygoi.Location = new System.Drawing.Point(220, 172);
            this.txbngaygoi.Name = "txbngaygoi";
            this.txbngaygoi.Size = new System.Drawing.Size(189, 20);
            this.txbngaygoi.TabIndex = 8;
            // 
            // txbsophut
            // 
            this.txbsophut.Location = new System.Drawing.Point(220, 198);
            this.txbsophut.Name = "txbsophut";
            this.txbsophut.Size = new System.Drawing.Size(189, 20);
            this.txbsophut.TabIndex = 9;
            // 
            // btnthem
            // 
            this.btnthem.Location = new System.Drawing.Point(479, 106);
            this.btnthem.Name = "btnthem";
            this.btnthem.Size = new System.Drawing.Size(75, 23);
            this.btnthem.TabIndex = 10;
            this.btnthem.Text = "Thêm mới";
            this.btnthem.UseVisualStyleBackColor = true;
            this.btnthem.Click += new System.EventHandler(this.btnthem_Click);
            // 
            // btnsua
            // 
            this.btnsua.Location = new System.Drawing.Point(479, 135);
            this.btnsua.Name = "btnsua";
            this.btnsua.Size = new System.Drawing.Size(75, 23);
            this.btnsua.TabIndex = 11;
            this.btnsua.Text = "Sửa";
            this.btnsua.UseVisualStyleBackColor = true;
            this.btnsua.Click += new System.EventHandler(this.btnsua_Click);
            // 
            // btnxoa
            // 
            this.btnxoa.Location = new System.Drawing.Point(479, 164);
            this.btnxoa.Name = "btnxoa";
            this.btnxoa.Size = new System.Drawing.Size(75, 23);
            this.btnxoa.TabIndex = 12;
            this.btnxoa.Text = "Xóa";
            this.btnxoa.UseVisualStyleBackColor = true;
            this.btnxoa.Click += new System.EventHandler(this.btnxoa_Click);
            // 
            // btnthoat
            // 
            this.btnthoat.Location = new System.Drawing.Point(479, 193);
            this.btnthoat.Name = "btnthoat";
            this.btnthoat.Size = new System.Drawing.Size(75, 23);
            this.btnthoat.TabIndex = 13;
            this.btnthoat.Text = "Thoát";
            this.btnthoat.UseVisualStyleBackColor = true;
            this.btnthoat.Click += new System.EventHandler(this.btnthoat_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(115, 201);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Số phút";
            // 
            // lstview
            // 
            this.lstview.BackColor = System.Drawing.SystemColors.MenuBar;
            this.lstview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cl1,
            this.cl2,
            this.cl3,
            this.cl4,
            this.cl5});
            this.lstview.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lstview.LabelEdit = true;
            this.lstview.Location = new System.Drawing.Point(42, 233);
            this.lstview.Name = "lstview";
            this.lstview.Size = new System.Drawing.Size(539, 158);
            this.lstview.TabIndex = 15;
            this.lstview.UseCompatibleStateImageBehavior = false;
            this.lstview.View = System.Windows.Forms.View.Details;
            this.lstview.VirtualListSize = 30;
            this.lstview.SelectedIndexChanged += new System.EventHandler(this.lstview_SelectedIndexChanged);
            // 
            // cl1
            // 
            this.cl1.Text = "Chi nhánh";
            this.cl1.Width = 100;
            // 
            // cl2
            // 
            this.cl2.Text = "Số gọi đi";
            this.cl2.Width = 100;
            // 
            // cl3
            // 
            this.cl3.Text = "Số gọi đến";
            this.cl3.Width = 100;
            // 
            // cl4
            // 
            this.cl4.Text = "Ngày gọi ";
            this.cl4.Width = 100;
            // 
            // cl5
            // 
            this.cl5.Text = "Số phút";
            this.cl5.Width = 136;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 384);
            this.Controls.Add(this.lstview);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnthoat);
            this.Controls.Add(this.btnxoa);
            this.Controls.Add(this.btnsua);
            this.Controls.Add(this.btnthem);
            this.Controls.Add(this.txbsophut);
            this.Controls.Add(this.txbngaygoi);
            this.Controls.Add(this.txbsogoiden);
            this.Controls.Add(this.sogoidi_cbb);
            this.Controls.Add(this.chinhanh_cbb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Thông tin cuộc gọi";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox chinhanh_cbb;
        private System.Windows.Forms.ComboBox sogoidi_cbb;
        private System.Windows.Forms.TextBox txbsogoiden;
        private System.Windows.Forms.TextBox txbngaygoi;
        private System.Windows.Forms.TextBox txbsophut;
        private System.Windows.Forms.Button btnthem;
        private System.Windows.Forms.Button btnsua;
        private System.Windows.Forms.Button btnxoa;
        private System.Windows.Forms.Button btnthoat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView lstview;
        private System.Windows.Forms.ColumnHeader cl1;
        private System.Windows.Forms.ColumnHeader cl2;
        private System.Windows.Forms.ColumnHeader cl3;
        private System.Windows.Forms.ColumnHeader cl4;
        private System.Windows.Forms.ColumnHeader cl5;
    }
}

