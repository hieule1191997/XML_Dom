﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
using System.Xml;

namespace BUS
{
    public class xml_BUS
    {
        XML_DTO dto = new XML_DTO();
        xml_DAL dal = new xml_DAL();
        public void them(string chinhanh, string sogoidi, string sogoiden, string ngaygoi, string sophut)
        {
            dto.doc = new XmlDocument();
            dto.doc.Load(dal.path);


            dto.cuocGoi = dto.doc.CreateElement("cuocgoi");
            dto.chiNhanh = dto.doc.CreateAttribute("chinhanh");
            dto.soDien = dto.doc.CreateAttribute("sodien");
            dto.sogoiDen = dto.doc.CreateElement("sogoiden");
            dto.ngayGoi = dto.doc.CreateElement("ngaygoi");
            dto.soPhut = dto.doc.CreateElement("sophut");

            dto.chiNhanh.InnerText = chinhanh;
            dto.soDien.InnerText = sogoidi;
            dto.sogoiDen.InnerText = sogoiden;
            dto.ngayGoi.InnerText = ngaygoi;
            dto.soPhut.InnerText = sophut;

            dto.cuocGoi.SetAttributeNode(dto.chiNhanh);
            dto.cuocGoi.SetAttributeNode(dto.soDien);
            dto.cuocGoi.AppendChild(dto.sogoiDen);
            dto.cuocGoi.AppendChild(dto.ngayGoi);
            dto.cuocGoi.AppendChild(dto.soPhut);
            dto.doc.DocumentElement.AppendChild(dto.cuocGoi);

            dto.doc.Save(dal.path);
           // MessageBox.Show("Bạn đã thêm thành công thông tin cuộc gọi mới", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        // sửa thông tin cuộc gọi
        public void sua(string chinhanh,string sogoidi , string sogoiden,string ngaygoi,string sophut)
        {
            dto.doc = new XmlDocument();
            dto.doc.Load(dal.path);
            XmlNode node = dto.doc.SelectSingleNode("/thongtincg/cuocgoi[sogoiden = '" + sogoiden.Trim() + "']");
            if (node != null)
            {
                node.Attributes[0].InnerText = chinhanh;
                node.Attributes[1].InnerText = sogoidi;

                node.ChildNodes[1].InnerText = ngaygoi;
                node.ChildNodes[2].InnerText = sophut;
                dto.doc.Save(dal.path);
                //MessageBox.Show("Bạn đã sửa thông tin cuộc gọi thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           


        }
        // xóa một cuộc gọi 
        public void xoa(string sogoiden)
        {
            dto.doc = new XmlDocument();
            dto.doc.Load(dal.path);
            XmlNode node = dto.doc.SelectSingleNode("/thongtincg/cuocgoi[sogoiden = '" + sogoiden.Trim() + "']");
            if (node != null)
            {
                dto.doc.DocumentElement.RemoveChild(node);
                dto.doc.Save(dal.path);
                //MessageBox.Show("Bạn đã xoá thông tin cuộc gọi thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
