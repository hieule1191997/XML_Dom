﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DTO
{
    public class XML_DTO
    {
       public XmlDocument doc;
           XmlElement cuocgoi, sogoiden, ngaygoi, sophut;
        XmlAttribute chinhanh, sodien;
        public XmlElement cuocGoi { get; set; }
        public XmlElement sogoiDen { get; set; }
        public XmlElement ngayGoi { get; set; }
        public XmlElement soPhut { get; set; }
        public XmlAttribute chiNhanh { get; set; }
        public XmlAttribute soDien { get; set; }
        
    }
}
